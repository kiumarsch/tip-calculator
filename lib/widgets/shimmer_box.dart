import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:tip_calculator/shimmer.dart';

class ShimmerEffect extends StatelessWidget {
  const ShimmerEffect({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      enabled: true,
      period: Duration(milliseconds: 800),
      baseColor: Colors.grey.shade300,
      highlightColor: Colors.grey.shade100,
      child: ShimmerBox(),
    );
  }
}

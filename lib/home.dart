import 'package:flutter/material.dart';
import 'package:tip_calculator/widgets/shimmer_box.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<bool> _tipOptions = [true, false, false, false];
  var controller = TextEditingController();

  void updateSelection(int selectedIndex) {
    for (int i = 0; i < _tipOptions.length; i++) {
      _tipOptions[i] = selectedIndex == i;
      print(_tipOptions);
      //the selectedindex==i is a bool determining if an index in a list is equal to the button you selected if yes then its true
    }
    setState(() {});
  }

  var tip;
  void calculateTip() {
    final billAmount = double.parse(controller.text);
    print(billAmount);
    var selectedIndex = _tipOptions.indexWhere((index) => index == true);
    print('selectedIndex is $selectedIndex');
    // finding where in a list the index is true
    final tipPrecentage = [0.05, 0.1, 0.15, 0.2][selectedIndex];
    print(tipPrecentage);
    final tipAmount = (billAmount * tipPrecentage).toStringAsFixed(2);
    print(tipAmount);
    setState(() {
      tip = tipAmount;
      print(tip);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: <Color>[
                Color.fromRGBO(233, 100, 67, 1),
                Color.fromRGBO(144, 78, 149, 1)
              ]),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (tip != null) Text('\$$tip'),
              // ShimmerEffect(),
              Text('Bill Amount'),
              SizedBox(
                  width: 70,
                  child: TextField(
                    controller: controller,
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(hintText: "10.00\$"),
                  )),
              Padding(
                padding: EdgeInsets.all(20),
                child: ToggleButtons(
                  borderWidth: 3,
                  children: [
                    Text('5%'),
                    Text('10%'),
                    Text('15%'),
                    Text('20%'),
                  ],
                  isSelected: _tipOptions,
                  onPressed: updateSelection,
                ),
              ),
              SizedBox(
                height: 50,
                width: 150,
                child: ElevatedButton(
                  onPressed: calculateTip,
                  child: Text('Calulate Tip'),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
